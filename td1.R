#
# This is a Shiny web application. You can run the application by clicking
# the 'Run App' button above.
#
# Find out more about building applications with Shiny here:
#
#    http://shiny.rstudio.com/
#
# ----------------------------------------------------------------------------

# Load packages
# ----------------------------------------------------------------------------
library(shiny)
library(dplyr)
library(lubridate)

# Load data
# ----------------------------------------------------------------------------

hospital_stay_src = read.csv2("data/hospital_stay.csv")
str(hospital_stay_src)
hospital_stay = hospital_stay_src %>%
    mutate(admission_date = as.character(ymd(admission_date)),
           discharge_date = as.character(ymd(discharge_date)))
str(hospital_stay)

hospital = read.csv2("data/hospital.csv")
str(hospital)



# Data management
# ----------------------------------------------------------------------------




# User Interface
# ----------------------------------------------------------------------------

# Define UI for application that draws a histogram
ui <- fluidPage(

    # Application title
    titlePanel("TD 1 - Printing records according to a filter"),

    # Sidebar with a slider input for number of bins 
    sidebarLayout(
        sidebarPanel(
            selectInput(
                inputId= "select_hospital", "Unit",
                choices= c("All", hospital$hospital)
            )
        ),

        # Show a plot of the generated distribution
        mainPanel(
            tableOutput("selected_hospital_stay")
        )
    )
)

# Server
# ----------------------------------------------------------------------------

# Define server logic required to draw a histogram
server <- function(input, output) {

    output$selected_hospital_stay <- renderTable({
        
        p = hospital_stay %>% 
            merge(hospital, id = "hospital_id") %>%
            filter(hospital == input$select_hospital) %>%
            select(hospital_id, admission_date, discharge_date, hospital)
        print(p)
        return(p)

    })
}


# Web application
# ----------------------------------------------------------------------------

# Run the application 
shinyApp(ui = ui, server = server)
